/* Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

/* Copyright (c) 2022, WheelBoard
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "qti_robot_car_ctrl.h"
#include "qti_robot_car_util.h"

sensor_msgs::Imu Mpu6050;//Instantiate an IMU object

/* Function: The main function, ROS initialization, creates the Robot_control object
 * through the Robot_Car class and automatically calls the constructor initialization
 */
int main(int argc, char** argv)
{
    ros::init(argc, argv, "robot_car"); //ROS initializes and sets the node name
    Robot_Car Robot_Control; //Instantiate an object
    Robot_Control.Control_Loop(); //Loop through data collection and publish the topic

    return 0;
}

/* Function: Data conversion function */
short Robot_Car::IMU_Trans(uint8_t Data_High,uint8_t Data_Low)
{
    short transition_16;
    transition_16 = 0;
    transition_16 |=  Data_High<<8;
    transition_16 |=  Data_Low;
    return transition_16;
}

float Robot_Car::Odom_Trans(uint8_t Data_High,uint8_t Data_Low)
{
    float data_return;
    short transition_16;
    transition_16 = 0;
    transition_16 |=  Data_High<<8;  //Get the high 8 bits of data
    transition_16 |=  Data_Low;      //Get the lowest 8 bits of data
    data_return   =  (transition_16 / 1000) + (transition_16 % 1000)*0.001; // The speed unit is changed from mm/s to m/s
    return data_return;
}

/* Function: Serial port communication check function, packet n has a byte, the NTH -1 byte
 * is the check bit, the NTH byte bit frame end.Bit XOR results from byte 1 to byte n-2 are
 * compared with byte n-1, which is a BCC check
 * Input parameter: Count_Number: Check the first few bytes of the packet
 */
unsigned char Robot_Car::Check_Sum(unsigned char Count_Number,unsigned char mode)
{
    unsigned char check_sum=0, k;

    if(mode == 0) //Receive data mode
    {
        for(k=0; k < Count_Number; k++)
        {
            check_sum = check_sum^Receive_Data.rx[k]; //By bit or by bit
        }
    }
    if(mode == 1) //Send data mode
    {
        for(k = 0; k < Count_Number; k++)
        {
            check_sum = check_sum^Send_Data.tx[k]; //By bit or by bit
        }
    }
    return check_sum; //Returns the bitwise XOR result
}

/* Function: The speed topic subscription Callback function, according to the subscribed
 * instructions through the serial port command control of the lower computer
 */
void Robot_Car::Cmd_Vel_Callback(const geometry_msgs::Twist &twist_aux)
{
    short  transition;  //intermediate variable
    Send_Data.tx[0] = FRAME_HEADER; //frame head 0x7B
    Send_Data.tx[1] = 0; //set aside
    Send_Data.tx[2] = 0; //set aside

    /* The target velocity of the X-axis of the robot */
    transition = 0;
    transition = twist_aux.linear.x*1000;
    Send_Data.tx[4] = transition;
    Send_Data.tx[3] = transition>>8;

    /* The target velocity of the Y-axis of the robot */
    transition = 0;
    transition = twist_aux.linear.y*1000;
    Send_Data.tx[6] = transition;
    Send_Data.tx[5] = transition>>8;

    /* The target angular velocity of the robot's Z axis */
    transition = 0;
    transition = twist_aux.angular.z*1000;
    Send_Data.tx[8] = transition;
    Send_Data.tx[7] = transition >> 8;

    Send_Data.tx[9] = Check_Sum(9,SEND_DATA_CHECK); //For the BCC check bits, see the Check_Sum function
    Send_Data.tx[10] = FRAME_TAIL; //frame tail 0x7D
    try
    {
        MCU_Serial.write(Send_Data.tx,sizeof (Send_Data.tx)); //Sends data to the downloader via serial port
    }
    catch (serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to send data through serial port"); //If sending data fails, an error message is printed
    }
}

/* Function: Publish the IMU data topic */
void Robot_Car::Publish_ImuSensor()
{
    sensor_msgs::Imu Imu_Data_Pub; //Instantiate IMU topic data
    Imu_Data_Pub.header.stamp = ros::Time::now();
    /* IMU corresponds to TF coordinates, which is required to use the robot_pose_ekf feature pack */
    Imu_Data_Pub.header.frame_id = gyro_frame_id;

    Imu_Data_Pub.orientation.x = Mpu6050.orientation.x; //A quaternion represents a three-axis attitude
    Imu_Data_Pub.orientation.y = Mpu6050.orientation.y;
    Imu_Data_Pub.orientation.z = Mpu6050.orientation.z;
    Imu_Data_Pub.orientation.w = Mpu6050.orientation.w;
    Imu_Data_Pub.orientation_covariance[0] = 1e6; //Three-axis attitude covariance matrix
    Imu_Data_Pub.orientation_covariance[4] = 1e6;
    Imu_Data_Pub.orientation_covariance[8] = 1e-6;
    Imu_Data_Pub.angular_velocity.x = Mpu6050.angular_velocity.x; //Triaxial angular velocity
    Imu_Data_Pub.angular_velocity.y = Mpu6050.angular_velocity.y;
    Imu_Data_Pub.angular_velocity.z = Mpu6050.angular_velocity.z;
    Imu_Data_Pub.angular_velocity_covariance[0] = 1e6; //Triaxial angular velocity covariance matrix
    Imu_Data_Pub.angular_velocity_covariance[4] = 1e6;
    Imu_Data_Pub.angular_velocity_covariance[8] = 1e-6;
    Imu_Data_Pub.linear_acceleration.x = Mpu6050.linear_acceleration.x; //Triaxial acceleration
    Imu_Data_Pub.linear_acceleration.y = Mpu6050.linear_acceleration.y;
    Imu_Data_Pub.linear_acceleration.z = Mpu6050.linear_acceleration.z;

    imu_publisher.publish(Imu_Data_Pub); //Pub IMU topic
}

/* Function: Publish the odometer topic, Contains position, attitude, triaxial velocity, angular
 * velocity about triaxial, TF parent-child coordinates, and covariance matrix
 */
void Robot_Car::Publish_Odom()
{
    /* Convert the Z-axis rotation Angle into a quaternion for expression */
    geometry_msgs::Quaternion odom_quat = tf::createQuaternionMsgFromYaw(Robot_Pos.Z);

    nav_msgs::Odometry odom; //Instance the odometer topic data
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = odom_frame_id; // Odometer TF parent coordinates
    odom.pose.pose.position.x = Robot_Pos.X; //Position
    odom.pose.pose.position.y = Robot_Pos.Y;
    odom.pose.pose.position.z = Robot_Pos.Z;
    odom.pose.pose.orientation = odom_quat; //Posture, Quaternion converted by Z-axis rotation

    odom.child_frame_id = robot_frame_id; // Odometer TF subcoordinates
    odom.twist.twist.linear.x =  Robot_Vel.X; //Speed in the X direction
    odom.twist.twist.linear.y =  Robot_Vel.Y; //Speed in the Y direction
    odom.twist.twist.angular.z = Robot_Vel.Z; //Angular velocity around the Z axis

    /* There are two types of this matrix, which are used when the robot is at rest and when it is
     * moving.Extended Kalman Filtering officially provides 2 matrices for the robot_pose_ekf feature pack
     */
    if(Robot_Vel.X== 0&&Robot_Vel.Y== 0&&Robot_Vel.Z== 0)
    {
        memcpy(&odom.pose.covariance, odom_pose_covariance2, sizeof(odom_pose_covariance2)),
        memcpy(&odom.twist.covariance, odom_twist_covariance2, sizeof(odom_twist_covariance2));
    }
    else
    {
        memcpy(&odom.pose.covariance, odom_pose_covariance, sizeof(odom_pose_covariance)),
        memcpy(&odom.twist.covariance, odom_twist_covariance, sizeof(odom_twist_covariance));
    }

    odom_publisher.publish(odom); //Pub odometer topic
}

/* Function: Publish voltage-related information */
void Robot_Car::Publish_BatVoltage()
{
    std_msgs::Float32 voltage_msgs; //Define the data type of the power supply voltage publishing topic
    static float Count_Voltage_Pub = 0;

    if(Count_Voltage_Pub++ > 10)
    {
        Count_Voltage_Pub = 0;
        voltage_msgs.data = Power_voltage; //The power supply voltage is obtained
        voltage_publisher.publish(voltage_msgs); //Post the power supply voltage topic unit: V, volt
    }
}

/* Function: Constructor, executed only once, for initialization */
Robot_Car::Robot_Car():Sampling_Time(0),Power_voltage(0)
{
    /* Clear the data */
    memset(&Robot_Pos, 0, sizeof(Robot_Pos));
    memset(&Robot_Vel, 0, sizeof(Robot_Vel));
    memset(&Receive_Data, 0, sizeof(Receive_Data));
    memset(&Send_Data, 0, sizeof(Send_Data));
    memset(&Mpu6050_Data, 0, sizeof(Mpu6050_Data));

    ros::NodeHandle private_nh("~"); //Create a node handle

    /* Fixed serial port number */
    private_nh.param<std::string>("usart_port_name",  usart_port_name,  "/dev/ttyUSB0");
    /* Communicate baud rate 115200 to the lower machine */
    private_nh.param<int>        ("serial_baud_rate", serial_baud_rate, 115200);
    /* The odometer topic corresponds to the parent TF coordinate */
    private_nh.param<std::string>("odom_frame_id",    odom_frame_id,    "odom_combined");
    /* The odometer topic corresponds to sub-TF coordinates */
    private_nh.param<std::string>("robot_frame_id",   robot_frame_id,   "base_footprint");
    private_nh.param<std::string>("gyro_frame_id",    gyro_frame_id,    "gyro_link"); //IMU topics correspond to TF coordinates

    voltage_publisher = n.advertise<std_msgs::Float32>("PowerVoltage", 10); //Create a battery-voltage topic publisher
    odom_publisher    = n.advertise<nav_msgs::Odometry>("odom", 50); //Create the odometer topic publisher
    imu_publisher     = n.advertise<sensor_msgs::Imu>("imu", 20); //Create an IMU topic publisher

    /* Set the velocity control command callback function */
    Cmd_Vel_Sub     = n.subscribe("cmd_vel", 100, &Robot_Car::Cmd_Vel_Callback, this);

    ROS_INFO_STREAM("Data ready"); //Prompt message

    try
    {
        /* Attempts to initialize and open the serial port */
        MCU_Serial.setPort(usart_port_name); //Select the serial port number to enable
        MCU_Serial.setBaudrate(serial_baud_rate); //Set the baud rate
        serial::Timeout _time = serial::Timeout::simpleTimeout(2000); //Timeout
        MCU_Serial.setTimeout(_time);
        MCU_Serial.open(); //Open the serial port
    }
    catch (serial::IOException& e)
    {
        /* If opening the serial port fails, an error message is printed */
        ROS_ERROR_STREAM("robot_car can not open serial port,Please check the serial port cable! ");
    }
    if(MCU_Serial.isOpen())
    {
        ROS_INFO_STREAM("robot_car serial port opened"); //Serial port opened successfully
    }
}

/**************************************
Function: Destructor, executed only once and called by the system when an object ends its life cycle

***************************************/
Robot_Car::~Robot_Car()
{
    /* Sends the stop motion command to the lower machine before the Robot_Car object ends */
    Send_Data.tx[0] = FRAME_HEADER;
    Send_Data.tx[1] = 0;
    Send_Data.tx[2] = 0;

    /* The target velocity of the X-axis of the robot */
    Send_Data.tx[4] = 0;
    Send_Data.tx[3] = 0;

    /* The target velocity of the Y-axis of the robot */
    Send_Data.tx[6] = 0;
    Send_Data.tx[5] = 0;

    /* The target velocity of the Z-axis of the robot */
    Send_Data.tx[8] = 0;
    Send_Data.tx[7] = 0;
    Send_Data.tx[9] = Check_Sum(9,SEND_DATA_CHECK); //Check the bits for the Check_Sum function
    Send_Data.tx[10] = FRAME_TAIL;
    try
    {
        MCU_Serial.write(Send_Data.tx,sizeof (Send_Data.tx)); //Send data to the serial port
    }
    catch (serial::IOException& e)
    {
        ROS_ERROR_STREAM("Unable to send data through serial port"); //If sending data fails, an error message is printed
    }
    MCU_Serial.close(); //Close the serial port
    ROS_INFO_STREAM("Shutting down"); //Prompt message
}

/* Function: The serial port reads and verifies the data sent by the lower computer,
 * and then the data is converted to international units
 */
bool Robot_Car::Get_Sensor_Data()
{
    short transition_16 = 0, j = 0, Header_Pos = 0, Tail_Pos = 0; //Intermediate variable
    uint8_t Receive_Data_Pr[RECEIVE_DATA_SIZE] = {0}; //Temporary variable to save the data of the lower machine
    MCU_Serial.read(Receive_Data_Pr, sizeof (Receive_Data_Pr)); //Read the data sent by the lower computer through the serial port

    /* Record the position of the head and tail of the frame */
    for(j = 0; j < 24; j++)
    {
        if(Receive_Data_Pr[j] == FRAME_HEADER)
        Header_Pos=j;
        else if(Receive_Data_Pr[j] == FRAME_TAIL)
        Tail_Pos = j;
    }

    if(Tail_Pos == (Header_Pos+23))
    {
        /* If the end of the frame is the last bit of the packet, copy the packet directly to receive_data.rx */
        memcpy(Receive_Data.rx, Receive_Data_Pr, sizeof(Receive_Data_Pr));
    }
    else if(Header_Pos == (1+Tail_Pos))
    {
        /* If the header is behind the end of the frame, copy the packet to receive_data.rx after correcting the data location */
        for(j = 0; j < 24; j++)
        Receive_Data.rx[j] = Receive_Data_Pr[(j+Header_Pos)%24];
    }
    else
    {
        /* In other cases, the packet is considered to be faulty */
        return false;
    }

    Receive_Data.Frame_Header= Receive_Data.rx[0]; //The first part of the data is the frame header 0X7B
    Receive_Data.Frame_Tail= Receive_Data.rx[23];  //The last bit of data is frame tail 0X7D

    if (Receive_Data.Frame_Header == FRAME_HEADER ) //Judge the frame header
    {
        if (Receive_Data.Frame_Tail == FRAME_TAIL) //Judge the end of the frame
        {
            if (Receive_Data.rx[22] == Check_Sum(22,READ_DATA_CHECK)||(Header_Pos==(1+Tail_Pos))) //BCC check passes or two packets are interlaced
            {
                Receive_Data.Flag_Stop=Receive_Data.rx[1]; //set aside
                /* Get the speed of the moving chassis in the X direction */
                Robot_Vel.X = Odom_Trans(Receive_Data.rx[2],Receive_Data.rx[3]);
                Robot_Vel.Y = Odom_Trans(Receive_Data.rx[4],Receive_Data.rx[5]);
                /* Get the speed of the moving chassis in the Z direction */
                Robot_Vel.Z = Odom_Trans(Receive_Data.rx[6],Receive_Data.rx[7]);

                /* MPU6050 stands for IMU only and does not refer to a specific model. It can be either MPU6050 or MPU9250 */
                Mpu6050_Data.accele_x_data = IMU_Trans(Receive_Data.rx[8],Receive_Data.rx[9]);   //Get the X-axis acceleration of the IMU
                Mpu6050_Data.accele_y_data = IMU_Trans(Receive_Data.rx[10],Receive_Data.rx[11]); //Get the Y-axis acceleration of the IMU
                Mpu6050_Data.accele_z_data = IMU_Trans(Receive_Data.rx[12],Receive_Data.rx[13]); //Get the Z-axis acceleration of the IMU
                Mpu6050_Data.gyros_x_data = IMU_Trans(Receive_Data.rx[14],Receive_Data.rx[15]);  //Get the X-axis angular velocity of the IMU
                Mpu6050_Data.gyros_y_data = IMU_Trans(Receive_Data.rx[16],Receive_Data.rx[17]);  //Get the Y-axis angular velocity of the IMU
                Mpu6050_Data.gyros_z_data = IMU_Trans(Receive_Data.rx[18],Receive_Data.rx[19]);  //Get the Z-axis angular velocity of the IMU

                /* Linear acceleration unit conversion is related to the range of IMU initialization of STM32, where the range is ±2g=19.6m/s^2 */
                Mpu6050.linear_acceleration.x = Mpu6050_Data.accele_x_data / ACCEl_RATIO;
                Mpu6050.linear_acceleration.y = Mpu6050_Data.accele_y_data / ACCEl_RATIO;
                Mpu6050.linear_acceleration.z = Mpu6050_Data.accele_z_data / ACCEl_RATIO;

                /* The gyroscope unit conversion is related to the range of STM32's IMU when initialized. Here, the range of IMU's gyroscope is ±500°/s */
                Mpu6050.angular_velocity.x =  Mpu6050_Data.gyros_x_data * GYROSCOPE_RATIO;
                Mpu6050.angular_velocity.y =  Mpu6050_Data.gyros_y_data * GYROSCOPE_RATIO;
                Mpu6050.angular_velocity.z =  Mpu6050_Data.gyros_z_data * GYROSCOPE_RATIO;

                /* Get the battery voltage */
                transition_16 = 0;
                transition_16 |=  Receive_Data.rx[20]<<8;
                transition_16 |=  Receive_Data.rx[21];
                Power_voltage = transition_16/1000+(transition_16 % 1000)*0.001; //Unit conversion millivolt(mv)->volt(v)

                return true;
            }
        }
    }
    return false;
}

/* Function: Loop access to the lower computer data and issue topics */
void Robot_Car::Control_Loop()
{
    _Last_Time = ros::Time::now();
    while(ros::ok())
    {
        _Now = ros::Time::now();
        Sampling_Time = (_Now - _Last_Time).toSec(); //Retrieves time interval, which is used to integrate velocity to obtain displacement (mileage)

        if (true == Get_Sensor_Data()) //The serial port reads and verifies the data sent by the lower computer, and then the data is converted to international units
        {
            Robot_Pos.X += (Robot_Vel.X * cos(Robot_Pos.Z) - Robot_Vel.Y * sin(Robot_Pos.Z)) * Sampling_Time; //Calculate the displacement in the X direction, unit: m
            Robot_Pos.Y += (Robot_Vel.X * sin(Robot_Pos.Z) + Robot_Vel.Y * cos(Robot_Pos.Z)) * Sampling_Time; //Calculate the displacement in the Y direction, unit: m
            Robot_Pos.Z += Robot_Vel.Z * Sampling_Time; //The angular displacement about the Z axis, in rad

            /* Calculate the three-axis attitude from the IMU with the angular velocity around the three-axis and the three-axis acceleration */
            Quaternion_Solution(Mpu6050.angular_velocity.x, Mpu6050.angular_velocity.y, Mpu6050.angular_velocity.z,\
                                Mpu6050.linear_acceleration.x, Mpu6050.linear_acceleration.y, Mpu6050.linear_acceleration.z);

            Publish_Odom();      //Pub the speedometer topic
            Publish_ImuSensor(); //Pub the IMU topic
            Publish_BatVoltage();   //Pub the topic of power supply voltage
        }
        _Last_Time = _Now; //Record the time and use it to calculate the time interval
        ros::spinOnce();   //The loop waits for the callback function
    }
}
