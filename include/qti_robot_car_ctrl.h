/* Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */

/* Copyright (c) 2022, WheelBoard
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __QTI_ROBOT_CAR_CTRL__
#define __QTI_ROBOT_CAR_CTRL__

#include "ros/ros.h"
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <serial/serial.h>
#include <fcntl.h>
#include <stdbool.h>
#include <tf/transform_broadcaster.h>
#include <std_msgs/String.h>
#include <std_msgs/Float32.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <sensor_msgs/Imu.h>

using namespace std;

/* Macro definition */
#define SEND_DATA_CHECK   1          //Send data check flag bits
#define READ_DATA_CHECK   0          //Receive data to check flag bits
#define FRAME_HEADER      0X7B       //Frame head
#define FRAME_TAIL        0X7D       //Frame tail
#define RECEIVE_DATA_SIZE 24         //The length of the data sent by the lower computer
#define SEND_DATA_SIZE    11         //The length of data sent by ROS to the lower machine
#define PI                3.1415926f //PI

/* Relative to the range set by the IMU gyroscope, the range is ±500°, corresponding data range is ±32768
 * The gyroscope raw data is converted in radian (rad) units, 1/65.5/57.30=0.00026644
 */
#define GYROSCOPE_RATIO   0.00026644f

/* Relates to the range set by the IMU accelerometer, range is ±2g, corresponding data range is ±32768
 * Accelerometer original data conversion bit m/s^2 units, 32768/2g=32768/19.6=1671.84
 */
#define ACCEl_RATIO       1671.84f

extern sensor_msgs::Imu Mpu6050; //External variables, IMU topic data

/* Covariance matrix for speedometer topic data for robt_pose_ekf feature pack */
const double odom_pose_covariance[36] = {1e-3, 0, 0, 0, 0, 0,
                                         0, 1e-3, 0, 0, 0, 0,
                                         0, 0, 1e6, 0, 0, 0,
                                         0, 0, 0, 1e6, 0, 0,
                                         0, 0, 0, 0, 1e6, 0,
                                         0, 0, 0, 0, 0, 1e3 };

const double odom_pose_covariance2[36] = {1e-9, 0, 0, 0, 0, 0,
                                          0, 1e-3, 1e-9, 0, 0, 0,
                                          0, 0, 1e6, 0, 0, 0,
                                          0, 0, 0, 1e6, 0, 0,
                                          0, 0, 0, 0, 1e6, 0,
                                          0, 0, 0, 0, 0, 1e-9 };

const double odom_twist_covariance[36] = {1e-3, 0, 0, 0, 0, 0,
                                          0, 1e-3, 0, 0, 0, 0,
                                          0, 0, 1e6, 0, 0, 0,
                                          0, 0, 0, 1e6, 0, 0,
                                          0, 0, 0, 0, 1e6, 0,
                                          0, 0, 0, 0, 0, 1e3 };

const double odom_twist_covariance2[36] = {1e-9, 0, 0, 0, 0, 0,
                                           0, 1e-3, 1e-9, 0, 0, 0,
                                           0, 0, 1e6, 0, 0, 0,
                                           0, 0, 0, 1e6, 0, 0,
                                           0, 0, 0, 0, 1e6, 0,
                                           0, 0, 0, 0, 0, 1e-9 };

/* Data structure for speed and position */
typedef struct __Vel_Pos_Data_
{
    float X;
    float Y;
    float Z;
}Vel_Pos_Data;

/* The structure in which the lower computer sends data to the ROS */
typedef struct _RECEIVE_DATA_
{
    uint8_t rx[RECEIVE_DATA_SIZE];
    uint8_t Flag_Stop;
    unsigned char Frame_Header;
    float X_speed;
    float Y_speed;
    float Z_speed;
    float Power_Voltage;
    unsigned char Frame_Tail;
}RECEIVE_DATA;

/* The structure of the ROS to send data to the down machine */
typedef struct _SEND_DATA_
{
    uint8_t tx[SEND_DATA_SIZE];
    float X_speed;
    float Y_speed;
    float Z_speed;
    unsigned char Frame_Tail;
}SEND_DATA;

/* IMU data structure */
typedef struct __MPU6050_DATA_
{
    short accele_x_data;
    short accele_y_data;
    short accele_z_data;
    short gyros_x_data;
    short gyros_y_data;
    short gyros_z_data;
}MPU6050_DATA;

class Robot_Car
{
public:
    Robot_Car();  //Constructor
    ~Robot_Car(); //Destructor
    void Control_Loop();   //Loop control code
    serial::Serial MCU_Serial; //Declare a serial object

private:
    ros::NodeHandle n;           //Create a ROS node handle
    ros::Time _Now, _Last_Time;  //Time dependent, used for integration to find displacement (mileage)
    float Sampling_Time;         //Sampling time, used for integration to find displacement (mileage)

    ros::Subscriber Cmd_Vel_Sub; //Initialize the topic subscriber
    /* The speed topic subscribes to the callback function */
    void Cmd_Vel_Callback(const geometry_msgs::Twist &twist_aux);

    ros::Publisher odom_publisher, imu_publisher, voltage_publisher; //Initialize the topic publisher
    void Publish_Odom();      //Pub the speedometer topic
    void Publish_ImuSensor(); //Pub the IMU sensor topic
    void Publish_BatVoltage();   //Pub the power supply voltage topic

    /* Read motion chassis speed, IMU, power supply voltage data from serial port (ttyUSB) */
    bool Get_Sensor_Data();
    unsigned char Check_Sum(unsigned char Count_Number,unsigned char mode); //BBC check function
    short IMU_Trans(uint8_t Data_High,uint8_t Data_Low);  //IMU data conversion read
    float Odom_Trans(uint8_t Data_High,uint8_t Data_Low); //Odometer data is converted to read

    string usart_port_name, robot_frame_id, gyro_frame_id, odom_frame_id; //Define the related variables
    int serial_baud_rate;      //Serial communication baud rate
    RECEIVE_DATA Receive_Data; //The serial port receives the data structure
    SEND_DATA Send_Data;       //The serial port sends the data structure

    Vel_Pos_Data Robot_Pos;    //The position of the robot
    Vel_Pos_Data Robot_Vel;    //The speed of the robot
    MPU6050_DATA Mpu6050_Data; //IMU data
    float Power_voltage;       //Power supply voltage
};
#endif
